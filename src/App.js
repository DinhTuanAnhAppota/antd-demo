import 'antd/dist/antd.css';
import './App.css';
import { Alert, Button, DatePicker, Form, Input, Progress, Select, Table, Tag, TimePicker } from 'antd';
import { useEffect, useState } from 'react';
import { PoweroffOutlined, UserOutlined } from '@ant-design/icons';

function App() {
  const [loading, setLoading] = useState(false)
  const [showAlert, setshowAlert] = useState(false)
  const [dataSource, setDataSource] = useState([])
  const [alreadySelectedRows, setAlreadySelectedRows] = useState(["1", "3"])

  const onButtonClick = (e) => {
    console.log("Button Click")
    setLoading(true)
    setTimeout(() => {
      setLoading(false)
    }, 2000);
  }
  const fruit = ['Banana', 'Mango', 'Orange', 'Cherry']

  const onFinish = (e) => {
    console.log(e)
    setTimeout(() => {
      setshowAlert(true)
    }, 2000);
  }

  const data = [
    {
      name: 'Name 2',
      age: 20,
      address: 'Address 2',
      key: '2'
    },
    {
      name: 'Name 1',
      age: 10,
      address: 'Address 1',
      key: '1'
    },
    {
      name: 'Name 3',
      age: 30,
      address: 'Address 3',
      key: '3'
    },
  ]

  const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'key',
      render: name => {
        return <a>{name}</a>
      }
    },
    {
      title: 'Age',
      dataIndex: 'age',
      key: 'key',
      sorter: (a, b) => a.age - b.age
    },
    {
      title: 'Address',
      dataIndex: 'address',
      key: 'key'
    },
    {
      title: 'Graduated?',
      key: 'key',
      render: payload => {
        return <p>{payload.age > 20 ? 'True' : 'False'}</p>
      }
    },
  ]

  const columns1 = [
    {
      key: '1',
      title: 'ID',
      dataIndex: 'id'
    },
    {
      key: '2',
      title: 'User ID',
      dataIndex: 'userId'
    },
    {
      key: '3',
      title: 'Status',
      dataIndex: 'completed',
      render: (completed) => {
        return <p>{completed ? 'Completed' : 'In Progress'}</p>
      }
    }
  ]

  const columns2 = [
    {
      title: 'Student ID',
      dataIndex: 'id'
    },
    {
      title: 'Student Name',
      dataIndex: 'name'
    },
    {
      title: 'Student Grade',
      dataIndex: 'grade',
      render: (tag) => {
        const color = tag.includes('A') ? 'Green' : tag.includes('B') ? "blue" : "red"
        return <Tag color={color} key={tag}>{tag}</Tag>
      }
    },
  ]

  const dataSources = [
    {
      key: '1',
      id: 1,
      name: 'Student Name 1',
      grade: 'A+'
    },
    {
      key: '2',
      id: 2,
      name: 'Student Name 2',
      grade: 'A'
    },
    {
      key: '3',
      id: 3,
      name: 'Student Name 3',
      grade: 'B+'
    },
    {
      key: '4',
      id: 4,
      name: 'Student Name 4',
      grade: 'C'
    },
    {
      key: '5',
      id: 5,
      name: 'Student Name 5',
      grade: 'A'
    },
  ]

  useEffect(() => {
    setLoading(true)
    fetch("http://jsonplaceholder.typicode.com/todos")
      .then(response => response.json())
      .then(data => {
        setDataSource(data)
      }).catch(err => {
        console.log(err)
      }).finally(() => {
        setLoading(false)
      })
  }, [])

  return (
    <div className="App">
      <header className="App-header">
        {showAlert &&
          <Alert type='error' message='Error'
            description='There was an error on login'
            closable
          />
        }
        <Form onFinish={onFinish}>
          <Form.Item label="User Name" name="username">
            <Input placeholder="User name" required />
          </Form.Item>
          <Form.Item label="Password" name="password">
            <Input.Password placeholder="Password" required />
          </Form.Item>
          <Form.Item>
            <Button block type='primary' htmlType='submit'>Log in</Button>
          </Form.Item>
        </Form>
        <Table
          dataSource={data}
          columns={columns}
        />
        <Input.Search
          placeholder='Input'
          maxLength={10}
          prefix={<UserOutlined />}
          allowClear
        />
        <p>Which is your favorite fruit?</p>
        <Select placeholder='select fruit' style={{ width: '50%' }}>
          {fruit.map((fruit, index) => {
            return <Select.Option key={index} value={fruit} />
          })}
        </Select>
        <div style={{ top: 16 }}>
          <DatePicker disabled={false} />
          <DatePicker.RangePicker />
          <TimePicker />
        </div>
        <Progress percent={21} strokeColor='yellow' />
        <Progress percent={69} type='circle' />
        <Progress percent={91} strokeColor='red' type='line' status='exception' />
        <Progress percent={51} type='line' status='active' strokeWidth={20} />
        <Progress percent={69} type='circle' status='success' />
        <Progress percent={33} type='line' steps={3} />
        <Button type='primary'
          block
          loading={loading}
          icon={<PoweroffOutlined />}
          className='my-button'
          onClick={onButtonClick}
        >
          My First Button
        </Button>
      </header>
      <div>
        <Table
          loading={loading}
          dataSource={dataSource}
          columns={columns1}
        />
      </div>
      <div>
        <Table
          dataSource={dataSources}
          columns={columns2}
          rowSelection={{
            type: 'checkbox',
            selectedRowKeys: alreadySelectedRows,
            onChange: (keys) => {
              setAlreadySelectedRows(keys)
            },
            onSelect: (record) => {
              console.log({ record })
            },
            getCheckboxProps: (record) => ({
            }),
            //hideSelectAll: true
            selections: [
              Table.SELECTION_NONE,
              Table.SELECTION_ALL,
              Table.SELECTION_INVERT,
              {
                key: 'even',
                text: 'Select Even Rows',
                onSelect: (allKeys) => {
                  const selectedKeys = allKeys.filter(key => {
                    return key % 2 === 0
                  })
                  setAlreadySelectedRows(selectedKeys)
                }
              },
              {
                key: 'excellent',
                text: 'Select Students with Excellent Grads',
                onSelect: (allKeys) => {
                  const selectedKeys = allKeys.filter(key => {
                    const isExellent = dataSources.find(student => {
                      return student.key === key && student.grade
                    })
                    return isExellent
                  })
                  setAlreadySelectedRows(selectedKeys)
                }
              }
            ]
          }}
        />
      </div>
    </div>
  );
}

export default App;
